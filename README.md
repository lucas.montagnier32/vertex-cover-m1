### Contributeurs
- Nathan Deshayes
- Lucas Montagnier

### Exécution du projet:

Aller dans le dossier contenant le code source et dans le REPL julia:
- include("ProjetVertexCover.jl")


Pour tester, il suffit d'appeler une méthode de test suivante dans le REPL après avoir inclus le code source:

- test_approx(n::Int64) qui test pour chaque valeur de p l'algo 2Approx en fonction du nombre de sommets n

- test_BST(n::Int64) qui test pour chaque valeur de p l'algo BST en fonction du nombre de sommets n

- test_Kernel(n::Int64) qui test pour chaque valeur de p l'algo Kernel en fonction du nombre de sommets n

- test_by_line(n::Int64) qui test pour une valeur n tout les algos pour chaque valeur p

- test_by_col(n::Int64) qui test pour une valeur n l'algo approx puis BST puis Kernel
