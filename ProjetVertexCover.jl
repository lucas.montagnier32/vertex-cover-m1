
struct Graph
	n::Int64 # nombre de sommets
	A::Vector{Tuple{Int64,Int64}} # liste des arêtes
	M::Matrix{Int64} # matrice d'adjacence
end

# n = nombre de sommets, p = proba que (u,v) existe, O(n²)
function generateRandomGraph(n::Int64, p::Float64)::Graph
	matrixGraph::Matrix{Int64} = zeros(Int64, n, n)
	A::Vector{Tuple{Int64,Int64}} = []
	for i in 1:n
		for j in (i+1):n
			if(rand(Float64)<=p) # création d'une arete
				push!(A, (i, j) )
				matrixGraph[i,j] = 1
				matrixGraph[j,i] = 1
			end
		end
	end
	return Graph(n, A, matrixGraph)
end

# affichage d'un graphe
function printGraph(G::Graph)
	println("\n-----Graphe à ",G.n," sommets-----")
	println("Nombre d'arêtes: ",length(G.A))
	if G.n <= 10
		println("Arêtes: ",G.A)
		println("Matrice d'adjacence:")
		for i in 1:G.n
			for j in 1:G.n
				print(" ",G.M[i,j]," ")
			end
			println()
		end
	end
	degInfos = degMaxAndMoy(G)
	println("Degré max: ",degInfos[1])
	println("Degré moyen: ",degInfos[2])
	println("-----------------------------")
end

# trouve le degré max et le degré moyen d'un graphe
function degMaxAndMoy(G::Graph)::Tuple{Int64,Float64}
	degMax::Int64 = 0
	degSum::Int64 = 0
	degSommet::Int64 = 0
	for s in 1:G.n # pour chaque sommets
		for a in G.A # pour chaque arêtes
			if s==a[1] || s==a[2]
				degSommet += 1
			end
		end
		degMax = max(degSommet, degMax)
		degSum += degSommet
		degSommet = 0
	end
	return (degMax, degSum/G.n)
end



# l’algorithme de 2-approximation pour MIN VC
function Approx2(G::Graph)::Vector{Int64}
	Vprime::Vector{Int64} = []
	# E est un Set pour pouvoir utiliser la fonction delete!()
	E::Set{Tuple{Int64,Int64}} = Set(deepcopy(G.A))
	arete::Tuple{Int64,Int64} = (0,0)
	
	while(!isempty(E))
		arete = first(E)
		push!(Vprime ,arete[1], arete[2])
		for a in E # pour chaques arêtes dans E
			if(a[1]==arete[1] || a[1]==arete[2] || a[2]==arete[1] || a[2]==arete[2])
				delete!(E,a) # suppressoins des arêtes incidentes a arete[1] et arete[2]
			end
		end
	end
	
	return Vprime
end

# algo BST
function VCBST(G::Graph, k::Int64)::Bool
	return VCBST_rec(G.A, k)
end 

# algo BST partie récursive
function VCBST_rec(E::Vector{Tuple{Int64,Int64}}, k::Int64)::Bool
	if isempty(E)
		return true
	elseif k==0
		return false
	end
	arete::Tuple{Int64,Int64} = first(E)
	# u=arete[1], v=arete[2]
	Gmu::Vector{Tuple{Int64,Int64}} = []
	Gmv::Vector{Tuple{Int64,Int64}} = []
	for a in E # pour chaques arêtes dans E
		if a[1]!=arete[1] && a[2]!=arete[1]
			push!(Gmu, a)
		end
		if a[1]!=arete[2] && a[2]!=arete[2]
			push!(Gmv, a)
		end
	end
	return ( VCBST_rec(Gmu, k-1) || VCBST_rec(Gmv, k-1) )
end

# trouve la valeur optimal avec l'algo BST en recherche dichotomique
function dicho_VCBST(g::Graph, deb::Int64, fin::Int64)::Int64
	if deb>fin
		return deb
	end
	if deb==fin
		if VCBST(g, deb)
			return deb
		else
			return deb+1
		end
	end
	milieu = Int64(floor((deb+fin)/2))
	result = VCBST(g, milieu)
	if result
		return dicho_VCBST(g, deb, milieu-1)
	else
		return dicho_VCBST(g, milieu+1, fin)
	end
end


#-------------------------------------------------------------------
# donne le degré d'un sommet s dans le graphe G pour Kernel
function deg(M::Matrix{Int64}, s::Int64)::Int64
	count::Int64 = 0
	for i in 1:size(M)[1]
		if M[s,i]==1
			count += 1
		end
	end
	return count
end

# M est passé en référence et sera modifé en supprimant les aretes incidentes à s
function delete_incident_edges(M::Matrix{Int64}, s::Int64)
	for i in 1:size(M)[1] # suppr des aretes incidentes à s
		if M[s,i]==1
			M[s,i]=0
			M[i,s]=0
		end
	end
end

# vérifie si un ensemble de sommets est un VC du graphe représenté par M
function is_VC(M::Matrix{Int64}, sommets::Vector{Int64})::Bool
	for i in 1:size(M)[1]
		for j in (i+1):size(M)[1]
			if M[i,j]==1 && !(i in sommets) && !(j in sommets)
				return false
			end
		end
	end
	return true
end

# renvoie la première combinaison de tab de taille n étant un VC de M
function comb(M, tab::Vector{Int64},n::Int64)::Vector{Int64}
	deb::Vector{Int64} = []
	return comb_rec(M, deb,tab,n)
end

# renvoie la première combinaison étant un VC de M
function comb_rec(M, one_combi::Vector{Int64}, rest::Vector{Int64}, n::Int64)::Vector{Int64}
	if n==0
		if is_VC(M, one_combi)
			return one_combi
		else
			return []
		end
	else
		for i in 1:length(rest)
			one_combi_copy = deepcopy(one_combi)
			push!(one_combi_copy, rest[i])
			result = comb_rec(M, one_combi_copy, rest[i+1:length(rest)],n-1)
			if result !=  []
				return result
			end
		end
	end
	return []
end


# Algo Kernel (renvoie les sommets du VC et un booléen si le VC est possible)
function Kernel(G::Graph, k::Int64)
	degU::Int64 = 0
	Sol::Vector{Int64} = []
	M::Matrix{Int64} = deepcopy(G.M) # matrice d'adjacence
	v::Int64 = 0 # voisin
	again::Bool = true
	# première partie "kernelization"
	while again
		again = false
		for u in 1:G.n
			degU = deg(M,u)
			# règle VC1
			if degU==1 && k>0
				for i in 1:G.n # recherche du voisin
					if M[u,i]==1
						v = i
						push!(Sol, v)
					end
				end
				delete_incident_edges(M, v)
				k-=1
				again = true
			# règle VC2
			elseif degU>k && k>0
				push!(Sol, u)
				delete_incident_edges(M, u)
				k-=1
				again = true
			end
		end
	end
	if k==0
		return (Sol, is_VC(M, Sol))
	end
	# deuxième partie "brute force"
	remaining_vertices::Vector{Int64} = [n for n in 1:G.n if !(n in Sol)]
	good_combi::Vector{Int64} = comb(M, remaining_vertices, k)
	if good_combi == []
		return ([],false)
	else
		return (append!(Sol,good_combi),true)
	end
end


# trouve la valeur optimal avec l'algo Kernel en recherche dichotomique
function dicho_Kernel(g::Graph, deb::Int64, fin::Int64)
	if deb>fin
		return deb
	end
	if deb==fin
		if Kernel(g, deb)[2]
			return deb
		else
			return deb+1
		end
	end
	milieu = Int64(floor((deb+fin)/2))
	result = Kernel(g, milieu)
	if result[2]
		return dicho_Kernel(g, deb, milieu-1)
	else
		return dicho_Kernel(g, milieu+1, fin)
	end
end


#-------------------------------------------------------------------
# test pour chaque valeur de p l'algo 2Approx en fonction du nombre de sommets
function test_approx(n::Int64)
	for i in 3:5
		g = generateRandomGraph(n, i/n)
		printGraph(g)
		println("approx = ",length(Approx2(g)))
	end
	g = generateRandomGraph(n, 0.1)
	printGraph(g)
	println("approx = ",length(Approx2(g)))
	g = generateRandomGraph(n, 0.2)
	printGraph(g)
	println("approx = ",length(Approx2(g)))
end

# test pour chaque valeur de p l'algo BST en fonction du nombre de sommets
function test_BST(n::Int64)
	for i in 3:5
		g = generateRandomGraph(n, i/n)
		printGraph(g)
		approx = length(Approx2(g))
		println("approx = ",approx)
		println("BST result = ", dicho_VCBST(g, Int64(approx/2), approx))
	end
	g = generateRandomGraph(n, 0.1)
	printGraph(g)
	approx = length(Approx2(g))
	println("approx = ",approx)
	println("BST result = ", dicho_VCBST(g, Int64(approx/2), approx))
	g = generateRandomGraph(n, 0.2)
	printGraph(g)
	approx = length(Approx2(g))
	println("approx = ",approx)
	println("BST result = ", dicho_VCBST(g, Int64(approx/2), approx))
end

# test pour chaque valeur de p l'algo Kernel en fonction du nombre de sommets
function test_Kernel(n::Int64)
	for i in 3:5
		g = generateRandomGraph(n, i/n)
		printGraph(g)
		approx = length(Approx2(g))
		println("approx = ",approx)
		println("Kernel result = ", dicho_Kernel(g, Int64(approx/2), approx))
	end
	g = generateRandomGraph(n, 0.1)
	printGraph(g)
	approx = length(Approx2(g))
	println("approx = ",approx)
	println("Kernel result = ", dicho_Kernel(g, Int64(approx/2), approx))
	g = generateRandomGraph(n, 0.2)
	printGraph(g)
	approx = length(Approx2(g))
	println("approx = ",approx)
	println("Kernel result = ", dicho_Kernel(g, Int64(approx/2), approx))
end

# test pour une valeur n tout les algos pour chaque valeur p
function test_by_line(n::Int64)
	for i in 3:5
		g = generateRandomGraph(n, i/n)
		printGraph(g)
		approx = length(Approx2(g))
		println("approx = ",approx)
		println("BST result = ", dicho_VCBST(g, Int64(approx/2), approx))
		println("Kernel result = ", dicho_Kernel(g, Int64(approx/2), approx))
	end
	g = generateRandomGraph(n, 0.1)
	printGraph(g)
	approx = length(Approx2(g))
	println("approx = ",approx)
	println("BST result = ", dicho_VCBST(g, Int64(approx/2), approx))
	println("Kernel result = ", dicho_Kernel(g, Int64(approx/2), approx))
	g = generateRandomGraph(n, 0.2)
	printGraph(g)
	approx = length(Approx2(g))
	println("approx = ",approx)
	println("BST result = ", dicho_VCBST(g, Int64(approx/2), approx))
	println("Kernel result = ", dicho_Kernel(g, Int64(approx/2), approx))
end


# test pour une valeur n l'algo approx puis BST puis Kernel
function test_by_col(n::Int64)
	graphs = [generateRandomGraph(n, 3/n), generateRandomGraph(n, 4/n), generateRandomGraph(n, 5/n), generateRandomGraph(n, 0.1), generateRandomGraph(n, 0.2)]
	approx = zeros(Int64,5)
	for i in 1:5
		printGraph(graphs[i])
		approx[i] = length(Approx2(graphs[i]))
		println("approx = ",approx[i])
	end
	for i in 1:5
		println("BST result = ", dicho_VCBST(graphs[i], Int64(approx[i]/2), approx[i]))
	end
	for i in 1:5
		println("Kernel result = ", dicho_Kernel(graphs[i], Int64(approx[i]/2), approx[i]))
	end
end


